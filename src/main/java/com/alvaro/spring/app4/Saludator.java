package com.alvaro.spring.app4;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Saludator {
	
	@Value("${mensaje}")
	private String mensaje;
	
	public Saludator() {
		
		this.mensaje = "";
	}
	
	public Saludator(String str) {
		
		this.mensaje = str;
	}

	public void setMensaje(String str) {
		this.mensaje = str;
		
	}
	
	public String saludo() {
		return (mensaje == null) ? "No hay mensaje" : mensaje;
	}

}
