package com.alvaro.spring.app3;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/*
 * Se pude usar @Autowired con funciones o constructores que piden mas de una inyeccion
 *  @Autowired(required = false) no afecta en la ejecucio nque sea null por l oque n odevuelve excepcion
 *  @Nullable
 * 
 */
//@Component
@Service
public class PeliculasService {
	
	@Autowired  //->en la propiedad por norma general
	private PeliculaDAO peliculaDao;
	
	//@Autowired  ->en el constructor para atributos de tipo final
	public PeliculasService(PeliculaDAO peliculaDao) {
		
		this.peliculaDao = peliculaDao;
	}
	
	/*
	 * uso de @nullable para decir que puede ser null si n ose le inyecta nada
	@Autowired
	public PeliculasService(@Nullable PeliculaDAO peliculaDao) {
		
		this.peliculaDao = peliculaDao;
	}
	*/
	
	
	public PeliculasService() {
		
		peliculaDao = null;
	}
	
	
	//Obliga a tener que tratar la inyeccion, si no es asi saldras excepcion, se puede poner tambioen al elemento.
	@SuppressWarnings("deprecation")
	/*La @Required-Notación está especializada para decirle a Spring que esta propiedad debe ser inyectada por 
	 * la información proporcionada en el archivo de configuración XML y no a través de anotaciones. Y eso no
	 *  importa cuando usas la @Autowire-Notación.*/
	@Required
	//@Autowired Metodo setter  si tiene alguna foram especial de iniciarse
	public void setPeliculaDao(PeliculaDAO peliculaDao) {
		this.peliculaDao = peliculaDao;
	}
	
	public List<Pelicula> peliculasPorGenero(String genero){
		return peliculaDao
				.findall()
				.stream()
				.filter(p -> p.getGenero().equalsIgnoreCase(genero))
				.collect(Collectors.toCollection(ArrayList::new));
	}

	/**
	 * @Service Clase Servicio
	 * @Repository es un DAO Spring data Spring data Rest
	 * @Controller es un componente que gestiona peticiones
	 */

}
