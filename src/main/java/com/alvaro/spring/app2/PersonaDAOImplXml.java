package com.alvaro.spring.app2;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeMap;

public class PersonaDAOImplXml implements PersonaDAO{

	private TreeMap<String, Persona> personas;
	
	public PersonaDAOImplXml() {
		personas = new TreeMap<String, Persona>();
	}
	
	@Override
	public Persona findById(String id) {
		Persona p = personas.get(id);
		return p;
	}

	@Override
	public ArrayList<Persona> findall() {
		ArrayList<Persona> array = new ArrayList<Persona> ();
		Set<String> keys = personas.keySet(); 
		
		for(String key: keys) {
			array.add(personas.get(key));
		}
		
		return array;
	}

	@Override
	public void update(Persona p) {
		
		personas.put(p.getNombre(), p);
		
	}

	@Override
	public void delete(String id) {
		personas.remove(id);
		
	}

	@Override
	public void insert(Persona p) {
		
		personas.put(p.getNombre(), p);

		
	}

	//Este metodo se ejjecutara al ser creadoel bean
	
	public void init() throws Exception {

		Persona p1 = new Persona("Alvaro", 24);
		Persona p2 = new Persona("David", 22);
		Persona p3 = new Persona("Jose", 22);
		Persona p4 = new Persona("Santi", 24);
		
		personas.put(p1.getNombre(), p1);
		personas.put(p2.getNombre(), p2);
		personas.put(p3.getNombre(), p3);
		personas.put(p4.getNombre(), p4);
		
	}

	
	public void destroy() throws Exception {
		System.out.println("Limpiando los datos de la lista");
		personas.clear();
		
	}

}
