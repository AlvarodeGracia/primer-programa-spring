package com.alvaro.spring.app3;

import java.util.Collection;

public interface CatalogoPeliculas {
	public Collection<Pelicula> getPeliculas();
}
