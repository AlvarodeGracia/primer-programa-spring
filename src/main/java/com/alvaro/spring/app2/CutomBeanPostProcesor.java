package com.alvaro.spring.app2;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class CutomBeanPostProcesor implements BeanPostProcessor {

	
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		System.out.println(beanName+": BEFORE Se ha iniciado satisfactoriamente");
		return BeanPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		System.out.println(beanName+": AFTER Se ha iniciado satisfactoriamente");
		return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
	}

	
	
}
