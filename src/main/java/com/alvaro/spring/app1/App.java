package com.alvaro.spring.app1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/*##### Clase de Inicio
 * Esta calse es la clase Aplicacion, donde empezara todo nuestro programa y llevara el metodo main
 * 
 * Spring funciona por beans, que son clases pque funcionan por la Inyencion de independencia
 * 
 * Para activar estos beans usamos la interfaz ApplicationContext con la que accederemos a estos beans
 * 
 * La foram de cargar estos beans es de multiples formas, XML, Anotaciones y Java
 * 
 * En este ejemplo los beans son creados por XML, por ello usamos la clase  ClassPathXmlApplicationContext
 * 
 */

public class App {
	
	public static void main(String[] args) {
		
		//Esto es el inicio de la aplicacion Spring
		//ApplicationContext seria nuestra app Spring
		//ClassPathXmlApplicationContext se encarga de definir inicar los bean a partir de un documento XML
		ApplicationContext appContext = new ClassPathXmlApplicationContext("beans.xml");
		
		//Delcaracion de un bean
		Saludator saludator = null;
		
		//Formas de instanciar un bean
		//1 getBean  con ID y casting
		saludator = (Saludator) appContext.getBean("saludator-spanish");
		
		//2. getBean con ID y tipo
		//saludator = appContext.getBean("saludator", Saludator.class);
		
		//3. getBean con tipo
		//saludator = appContext.getBean(Saludator.class);
		
		//Llamamos a un metodo del bean
		System.out.println(saludator.saludo());
		
		//Cargamos ahora el bean EmailService con la forma 3, que es poniendo el nombre de la clase
		//Esto solo funcionara si solo hay una sola clase llamada asi dentro de los beans.
		EmailService emailService = null;
		emailService = appContext.getBean(EmailService.class);
		
		emailService.enviarSaludo("correo@gmail.com");
		
		
		//Cerramos la app de forma generica
		((ConfigurableApplicationContext) appContext).close();
		
		//((ClassPathXmlApplicationContext) appContext).close();
		
		//Esta forma de generar el Aplicatio ncontexts es para cuando el fichero beans esta en un lugar del sistema
		//ApplicationContext appContext = new FileSystemXmlApplicationContext("beans_fileSystem.xml");
		//((FileSystemXmlApplicationContext) appContext).close();
		
		//##### profucndizar en beans
	}

}
