package com.alvaro.spring.app4;

public class EmailServiceImpl implements EmailService{
	
	private Saludator saludator;
	
	public void setSaludator(Saludator saludator) {
		this.saludator = saludator;
	}
	
	@Override
	public void enviarSaludo(String str) {
		System.out.println("Enviando email a: " + str );
		System.out.println("Mensaje: "+saludator.saludo());
		
	}

}
