package com.alvaro.spring.app3;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;

public class AppConfigAllAnnotations {

	/*
	<bean id="peliculasService" 
		class="com.alvaro.spring.app3.PeliculasService" 
	/>*/
	
	//Inyecion 
	@Bean
	public PeliculasService peliculasService(PeliculaDAO peliculaDao) {
		return new PeliculasService(peliculaDao);
	}
	
	@Bean
	//@RequestScope web
	//@SesionScope web
	//@ApplicationScpoe web
	@Scope("singleton")
	public PeliculasDaoImpl peliculasDaoImpl() {
		return new PeliculasDaoImpl();
	}
	
	@Bean
	@Primary
	public CatalogoPeliculasActuales actuales() {
		return new CatalogoPeliculasActuales();
	}

	@Bean
	public CatalogoPeliculasClasicas clasicas() {
		return new CatalogoPeliculasClasicas();
	}
	
}
