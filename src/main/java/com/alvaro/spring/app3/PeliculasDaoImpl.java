package com.alvaro.spring.app3;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

//@Component
@Repository
public class PeliculasDaoImpl implements PeliculaDAO{

	private TreeMap<String, Pelicula> peliculas;
	
	@Autowired
	//@Epoca("clasicas")
	@Qualifier("actuales")
	private CatalogoPeliculas catalogoPeliculas;

	public void setCatalogoPeliculas(@Qualifier("catalogoPeliculasClasicas") CatalogoPeliculas catalogoPeliculas) {
		this.catalogoPeliculas = catalogoPeliculas;
	}

	public PeliculasDaoImpl() {
		peliculas = new TreeMap<String, Pelicula>();
	}
	
	@Override
	public Pelicula findById(String id) {
		Pelicula p = peliculas.get(id);
		return p;
	}

	@Override
	public ArrayList<Pelicula> findall() {
		ArrayList<Pelicula> array = new ArrayList<Pelicula> ();
		Set<String> keys = peliculas.keySet(); 
		
		for(String key: keys) {
			array.add(peliculas.get(key));
		}
		
		return array;
	}

	@Override
	public void update(Pelicula p) {
		
		peliculas.put(p.getTitulo(), p);
		
	}

	@Override
	public void delete(String id) {
		peliculas.remove(id);
		
	}

	@Override
	public void insert(Pelicula p) {
		
		peliculas.put(p.getTitulo(), p);

		
	}

	//Este metodo se ejjecutara al ser creadoel bean
	@PostConstruct
	public void init() throws Exception {
		
		
		for(Pelicula peli : catalogoPeliculas.getPeliculas()) {
			peliculas.put(peli.getTitulo(), peli);
			//System.out.println("A�adiendo peli: "+peli.getTitulo());
		}
		
		
		
	}

	@PreDestroy
	public void destroy() throws Exception {
		System.out.println("Limpiando los datos de la lista");
		peliculas.clear();
		
	}
	
	
}
