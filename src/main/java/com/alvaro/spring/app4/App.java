package com.alvaro.spring.app4;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
	
	public static void main(String[] args) {
		
		//Esto es el inicio de la aplicacion Spring
		//ApplicationContext seria nuestra app Spring
		//ClassPathXmlApplicationContext se encarga de definir inicar los bean a partir de un documento XML
		ApplicationContext appContext = new AnnotationConfigApplicationContext(AppConfig.class);
		//System.out.println("Hola");
		Saludator saludator = null;
		
		//1 getBean  con ID y casting
		//saludator = (Saludator) appContext.getBean("saludator-spanish");
		
		//2. getBean con ID y tipo
		//saludator = appContext.getBean("saludator", Saludator.class);
		
		//2. getBean con tipo
		//saludator = appContext.getBean(Saludator.class);
		
		
		//System.out.println(saludator.saludo());
		
		EmailService emailService = null;
		emailService = appContext.getBean(EmailService.class);
		
		emailService.enviarSaludo("correo@gmail.com");
		
		
		//Cerramos la app
		((AnnotationConfigApplicationContext) appContext).close();
		
		
		//ApplicationContext appContext = new FileSystemXmlApplicationContext("beans_fileSystem.xml");
		//((FileSystemXmlApplicationContext) appContext).close();
		
		//##### profucndizar en beans
	}

}
