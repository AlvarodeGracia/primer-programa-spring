package com.alvaro.spring.app3;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alvaro.spring.app2.PersonaDAO;

public class App {
	
	public static void main(String[] args) {
		
		//ApplicationContext appContext = new ClassPathXmlApplicationContext("beansAnotation.xml");
		
		ApplicationContext appContext = new AnnotationConfigApplicationContext(AppConfig.class);
		
		//Escanear por aspectos
		//((AnnotationConfigApplicationContext) appContext).scan("com.alvaro.spring.app3");
		//((AnnotationConfigApplicationContext) appContext).refresh();
		
		
		//Agregarlos poco a poco
		/*AnnotationConfigApplicationContext appContext = new AnnotationConfigApplicationContext(AppConfig.class);
		((AnnotationConfigApplicationContext) appContext).register(AppConfig.class);
		((AnnotationConfigApplicationContext) appContext).refresh();*/
		
		System.out.println("Peliculas de Ciencia ficci�n");
		PeliculasService personasservice = appContext.getBean(PeliculasService.class);
		personasservice.peliculasPorGenero("Ciencia ficci�n").forEach(System.out::println);
		
		//Cerramos la app
		((AnnotationConfigApplicationContext) appContext).close();
		
	}

}
