package com.alvaro.spring.app2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
	
	
	public static void main(String[] args) {
		
		ApplicationContext appContext = new ClassPathXmlApplicationContext("beans.xml");
		
		PersonaDAO personasDAO = appContext.getBean(PersonaDAO.class);
		personasDAO.findall().forEach(System.out::println);
		
		//Cerramos la app
		((ClassPathXmlApplicationContext) appContext).close();
		
	}

}
