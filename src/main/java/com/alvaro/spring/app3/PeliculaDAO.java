package com.alvaro.spring.app3;

import java.util.ArrayList;

public interface PeliculaDAO {
	
	public Pelicula findById(String id);
	public ArrayList<Pelicula> findall();
	public void update(Pelicula p);
	public void delete(String id);
	public void insert(Pelicula p);

}
