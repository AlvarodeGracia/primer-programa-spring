package com.alvaro.spring.app1;

public class Saludator {
	
	private String mensaje;
	
	public Saludator() {
		
		this.mensaje = "";
	}
	
	public Saludator(String str) {
		
		this.mensaje = str;
	}

	public void setMensaje(String str) {
		this.mensaje = str;
		
	}
	
	public String saludo() {
		return (mensaje == null) ? "No hay mensaje" : mensaje;
	}

}
