package com.alvaro.spring.app3;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
//Escanear con aspectis
//@ComponentScan(basePackages="com.alvaro.spring.app3")
public class AppConfig {
	
	
	/*
	<bean id="peliculasService" 
		class="com.alvaro.spring.app3.PeliculasService" 
	/>*/
	
	//Inyecion 
	@Bean
	public PeliculasService peliculasService(PeliculaDAO peliculaDao) {
		return new PeliculasService(peliculaDao);
	}
	
	@Bean
	public PeliculasDaoImpl peliculasDaoImpl() {
		return new PeliculasDaoImpl();
	}
	
	@Bean
	public CatalogoPeliculasActuales actuales() {
		return new CatalogoPeliculasActuales();
	}

	@Bean
	public CatalogoPeliculasClasicas clasicas() {
		return new CatalogoPeliculasClasicas();
	}
}
