package com.alvaro.spring.app2;

import java.util.ArrayList;

public interface PersonaDAO {
	
	public Persona findById(String id);
	public ArrayList<Persona> findall();
	public void update(Persona p);
	public void delete(String id);
	public void insert(Persona p);

}
