package com.alvaro.spring.app4;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:/ejemplo.properties")
public class AppConfig {
	
	@Bean
	public Saludator saludator() {
		return new Saludator();
	}
	
	@Bean
	public EmailServiceImpl emailService(Saludator saludator) {
		EmailServiceImpl emailService = new EmailServiceImpl();
		emailService.setSaludator(saludator);
		return emailService;
	}
	
}
